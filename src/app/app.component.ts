import { Component } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { PersonsService, IPerson } from './persons.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  columnsToDisplay = ['name', 'employer', 'value'];
  dataSource: MatTableDataSource<IPerson>;
  filter: string;
  personService: PersonsService;

  constructor(private service: PersonsService) {
    this.personService = service;
    this.personService.persons.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
    });
  }

  calculateTotals(): number {
    const data = this.dataSource ? this.dataSource.data : [];
    return data.reduce((accumulator, current) => accumulator + current.value, 0);
  }

  applyFilter(value: string): void {
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }

}

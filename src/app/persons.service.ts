import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PersonsService {
  data: IPerson[] = [
    { name: 'Leonardo', employer: 'CarTrawler', value: 150 },
    { name: 'Fabiano', employer: 'Techshop', value: 250 },
    { name: 'Vinicius', employer: 'RioGlass', value: 550 }
  ];

  constructor() { }

  get persons(): Observable<IPerson[]> {
    return new Observable(observer => {
      setTimeout(() => observer.next(this.data), 1000);
    });
  }
}

export interface IPerson {
  name: string;
  employer: string;
  value: number;
}
